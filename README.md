# RODARE Base Docker Image

This project aims to provide a basic docker image for RODARE in order to
improve the pipeline's duration.

The image is currently based on the `Python 3.5` docker image. It additionally
installs and builds the following components:

- libxml2-dev
- libxmlsec1-dev
- Node.js 8
- libtorrent 1.1.8 including Python 3 bindings

## Use the image from GitLab Registry

First log in to GitLab's Container Registry:

```bash
docker login gitlab.hzdr.de:5005
```

Afterwards you can run the container interactively with the following command:

```bash
docker run -it gitlab.hzdr.de:5005/rodare/rodare-docker-base
```

## Build the image on your own

To build the image on your own you need Docker installed on your system. To
build the image run the following command:

```bash
docker build -t rodare-docker-base .
```

To get a Python shell inside the docker container simply run:

```bash
docker run -it rodare-docker-base
```