FROM python:3.5

ARG TERM=linux
ARG DEBIAN_FRONTEND=noninteractive
ARG PYTHON=/usr/local/bin/python
ARG PYTHON_VERSION=3.5

# Install dependencies via apt-get
RUN apt-get update \
    && apt-get -qy upgrade --fix-missing --no-install-recommends \
    && apt-get -qy install --fix-missing --no-install-recommends \
        libxml2-dev libxmlsec1-dev build-essential libfreetype6-dev \
        autoconf libboost-system-dev libboost-chrono-dev libboost-random-dev \
        libboost-python-dev libboost-dev \
    # Install Node.js
    && curl -sL https://deb.nodesource.com/setup_8.x | bash - \
    && apt-get -qy install --fix-missing --no-install-recommends \
        nodejs

# Build libtorrent with python bindings.
RUN git clone https://github.com/arvidn/libtorrent.git \
    && cd libtorrent && git checkout libtorrent-1_1_8 \
    && ./autotool.sh && ./configure --enable-python-binding=yes --with-boost-python=boost_python3 \
    && make -j2 && make install \
    && python setup.py build \
    && python setup.py install \
    && cd ../ && rm -rf libtorrent/ \
    # Reduce image size
    && apt-get remove autoconf -y \
    && apt-get clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/{apt,dpkg}/ \
    && rm -rf /usr/share/man/* /usr/share/groff/* /usr/share/info/* \
    && find /usr/share/doc -depth -type f ! -name copyright -delete

ENV LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH
